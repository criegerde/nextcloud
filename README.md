# Nextcloud

Installieren, optimieren und härten Sie Ihren eigenen Nextcloud-Server und erweitern diesen nach Bedarf mit mehr Speicher, apticron, postfix u.v.m.

* Debian 11+
* Ubuntu 20.04+
* Ubuntu 22.04+
 
Weitere Optimierungs-, Härtungs- und Erweiterungsmöglichkeiten werden unter
https://www.c-rieger.de/nextcloud-installationsanleitung/
beschrieben. Viel Spaß.

Carsten Rieger IT-Services
https://www.c-rieger.de