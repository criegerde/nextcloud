#!/bin/bash
apt-mark unhold mariadb*
apt-mark unhold galera*
apt-mark unhold *mariadb*
apt-mark unhold mysql*
apt-mark unhold nginx*
apt-mark unhold php8.*
apt-mark unhold php*
apt-mark unhold redis*
exit 0
