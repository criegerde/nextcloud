#!/bin/bash
##############################################################
# ERSETZEN Sie zuerst:
# sdomain = ihre-signaling.domain.de
# ndomain = ihre-nextcloud-domain.de
# rsecret = recording-secret>
# isecret = internal-secret>
##############################################################
sdomain="ihre-signaling.domain.de"
ndomain="ihre-nextcloud.domain.de"
rsecret="abc...xyz"
isecret="def...tuv"
#-------------------------------------------#
# ### AB HIER BITTE NICHTS MEHR ÄNDERN! ### #
#-------------------------------------------#
natsdocker="docker run --name=NATSSERVER-NC -d -p 4222:4222 -ti --restart=always nats:latest"
recordingdocker="docker run -d --name NextcloudRecording -e HPB_DOMAIN=$sdomain -e NC_DOMAIN=$ndomain -e RECORDING_SECRET=$rsecret -e INTERNAL_SECRET=$isecret -e TZ=Europe/Berlin -p 127.0.0.1:1234:1234 --restart=always nextcloud/aio-talk-recording"
if [ -f /tmp/ncupdateskript ]; then
        clear
        clear
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo ""
        echo " » Das Updateskript ist bereits aktiv - *ABBRUCH*"
        echo " » Oder wurde ein vorheriger Prozess abgebrochen?"
        echo ""
        echo " » "$(ls /tmp/ncupdateskript)
        echo ""
        echo " » Entfernen Sie ggf. die Datei mit diesem Befehl:"
        echo " » sudo rm -f /tmp/ncupdateskript"
        echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo ""
        exit 1
fi
if [ "$USER" != "root" ]
then
    clear
    echo ""
    echo " » KEINE ROOT-BERECHTIGUNGEN | NO ROOT PERMISSIONS"
    echo ""
    echo "-------------------------------------------------------------"
    echo " » Bitte starten Sie das Skript als root: 'sudo ./update.sh'"
    echo " » Please run this script as root using:  'sudo ./update.sh'"
    echo "-------------------------------------------------------------"
    echo ""
    exit 1
fi
touch /tmp/ncupdateskript
clear
echo ""
apt update
apt-mark unhold nginx* nginx-* docker* docker-* janus* janus-*
apt upgrade -V
apt-mark hold nginx* nginx-* docker* docker-* janus* janus-*
apt autoremove
apt autoclean
clear
echo " » Nextcloud wird aktualisiert..."
echo ""
cd /etc
rm -rf nextcloud-spreed-signaling
git clone https://github.com/strukturag/nextcloud-spreed-signaling.git
cd nextcloud-spreed-signaling
make clean
make build
cp /etc/signaling/server.conf /etc/nextcloud-spreed-signaling/
sleep 3
echo ""
echo " » NATSSERVERAktualisierung..."
echo""
docker stop NATSSERVER-NC
docker stop NextcloudRecording
docker rm NATSSERVER-NC
docker rm NextcloudRecording
docker image rm $(docker image ls | grep nats | awk '{ print $3 }')
docker image rm $(docker image ls | grep recording | awk '{ print $3 }')
$natsdocker
$recordingdocker
sleep 5 
clear
echo ""
echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo ""
echo " » Dienste (nginx, janus, signaling) werden neu gestartet"
systemctl restart nginx.service janus.service signaling.service
echo ""
echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo ""
if [ -e /var/run/reboot-required ]; then
        echo -e " »\e[1;31m ACHTUNG: ES IST EIN SERVERNEUSTART ERFORDERLICH.\033[0m"
echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
else
        echo -e " »\033[32m  KEIN Serverneustart notwendig.\033[0m"
        echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
fi
echo ""
rm -f /tmp/ncupdateskript
exit 0
# (c) Carsten Rieger, https://www.c-rieger.de