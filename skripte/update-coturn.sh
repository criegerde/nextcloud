#!/bin/bash
apt update
apt upgrade -V
apt autoremove
apt autoclean
clear
echo ""
echo -n " » coturn Neustart gewünscht [y|n]?"
read answer
clear
echo ""
echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo ""
if [ "$answer" != "${answer#[YyjJ]}" ] ;then
/usr/bin/systemctl restart coturn.service
echo " » Dienste werden neu gestartet..."
else
    echo " » Sie wünschen keinen Neustart des coturn-Services."
fi
echo ""
echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo ""
if [ -e /var/run/reboot-required ]; then
        echo -e " »\e[1;31m ACHTUNG: ES IST EIN SERVERNEUSTART ERFORDERLICH.\033[0m"
	echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
else
	echo -e " »\033[32m  KEIN Serverneustart notwendig.\033[0m"
	echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
fi
echo ""
exit 0
# (c) Carsten Rieger, https://www.c-rieger.de