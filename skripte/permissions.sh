#!/bin/bash
find /var/www/ -type f -print0 | xargs -0 chmod 0640
find /var/www/ -type d -print0 | xargs -0 chmod 0750
chmod -R 775 /var/www/letsencrypt
chmod -R 770 /etc/letsencrypt 
chown -R www-data:www-data /var/www /etc/letsencrypt
chown -R www-data:www-data /Pfad/zum/Cloud-Datenverzeichnis
chmod 0644 /var/www/nextcloud/.htaccess
chmod 0644 /var/www/nextcloud/.user.ini
exit 0
