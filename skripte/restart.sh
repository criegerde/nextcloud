#!/bin/bash
echo "Neustart der Nextcloud-Services..."
systemctl stop nginx.service
systemctl restart mariadb.service redis-server.service php8.1-fpm.service
systemctl start nginx.service
exit 0