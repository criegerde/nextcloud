#!/bin/bash
apt-mark hold mariadb*
apt-mark hold galera*
apt-mark hold *mariadb*
apt-mark hold mysql*
apt-mark hold nginx*
apt-mark hold php8.*
apt-mark hold php*
apt-mark hold redis*
exit 0
