#!/bin/bash
export BORG_PASSPHRASE='IhrGeheimesBORG-SECRET'
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes
export BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
startTime=$(date +%s)
currentDate=$(date --date @"$startTime" +"%Y%m%d_%H%M%S")
currentDateReadable=$(date --date @"$startTime" +"%d.%m.%Y - %H:%M:%S")
logDirectory="/backup-logs/"
logFile="${logDirectory}/${currentDate}.log"
backupDiscMount="/borgbackup/cloud"
localBackupDir="/borgbackup/cloud/temp"
borgRepository="${backupDiscMount}/daten"
borgBackupDirs="/etc/ /root/ /var/www/nextcloud/ $localBackupDir/"
nextcloudFileDir='/var/www/nextcloud'
webserverServiceName='nginx'
webserverUser='www-data'
nextcloudDatabase='Datenbankname'
dbUser='Datenbankbenutzer'
dbPassword='DatenbankPasswort'
fileNameBackupDb='nextcloud.sql'
if [ ! -d "${logDirectory}" ]
then
	mkdir -p "${logDirectory}"
fi
errorecho() { cat <<< "$@" 1>&2; }
exec > >(tee -i "${logFile}")
exec 2>&1
if [ "$(id -u)" != "0" ]
then
	errorecho "ERROR: This script has to be run as root!"
	exit 1
fi
if [ ! -d "${localBackupDir}" ]
then
	errorecho "ERROR: The local backup directory ${localBackupDir} does not exist!"
	exit 1
fi
echo -e "\n###### Start des Backups: ${currentDateReadable} ######\n"
echo -e "Daten werden zusammengestellt"
dpkg --get-selections > "${localBackupDir}/software.list"
sudo -u "${webserverUser}" php ${nextcloudFileDir}/occ maintenance:mode --on
echo "nginx wird gestoppt"
systemctl stop "${webserverServiceName}"
echo "Datenbanksicherung wird erstellt"
mariadb-dump --single-transaction --routines -h localhost -u "${dbUser}" -p"${dbPassword}" "${nextcloudDatabase}" > "${localBackupDir}/${fileNameBackupDb}"
echo -e "\nBackup mit borgbackup"
borg create --stats \
    $borgRepository::"${currentDate}" \
	$localBackupDir \
	$borgBackupDirs 
echo
echo "nginx wird gestartet"
systemctl start "${webserverServiceName}"
sudo -u "${webserverUser}" php ${nextcloudFileDir}/occ maintenance:mode --off
rm "${localBackupDir}"/software.list
rm -r "${localBackupDir}/${fileNameBackupDb}"
borg prune --progress --stats $borgRepository --keep-within=7d --keep-weekly=4 --keep-monthly=6
endTime=$(date +%s)
endDateReadable=$(date --date @"$endTime" +"%d.%m.%Y - %H:%M:%S")
duration=$((endTime-startTime))
durationSec=$((duration % 60))
durationMin=$(((duration / 60) % 60))
durationHour=$((duration / 3600))
durationReadable=$(printf "%02d Stunden %02d Minuten %02d Sekunden" $durationHour $durationMin $durationSec)
echo -e "\n###### Ende des Backups: ${endDateReadable} (${durationReadable}) ######\n"
echo -e "Plattenbelegung:\n"
df -h ${backupDiscMount}
mail -s "Nextcloud-Backup - $(date --date @"$startTime" +"%d.%m.%Y")" -a "FROM: Ihr Name <ihre@emailadresse.de>" ihre@emailadresse.de < /Pfad/zur/Logdatei
