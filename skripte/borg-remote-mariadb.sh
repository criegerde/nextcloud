#!/usr/bin/env bash
#
# Nextcloud Sicherung: Hetzner-Storagebox
# Datenbanktyp: MariaDB
# 
###################################################################################################
export BORG_RSH='ssh -i /home/<user>/.ssh/id_ed25519'
# /home/<user>/.ssh/id_ed25519: Pfad zum privaten Schlüssel

export BORG_PASSPHRASE="SECRET-PASSPHRASE"
# SECRET-PASSPHRASE: Passphrase, mit dem das Repo erstellt wurde

VORNAME="Vorname"
# Vorname: Ihr Vorname

NACHNAME="Nachname"
# Nachname: Ihr Nachname

EMAIL="mail@domain.de"
# mail@domain.de: Ihre Emailadresse

NPATH="/var/www/nextcloud"
# NPATH: Pfad zur Nextlcoud-Software

WEBSERVER="nginx"
# WEBSERVER:  "nginx" oder "apache2"

PHPVERSION="8.2"
# PHPVERSION: "8.3" oder "8.2" oder "8.1"

BACKUP_USER="uxxxxxx"
# BACKUP_USER: Ihr Benutzer der Hetzner Storage Box

REPOSITORY_DIR="cloud"
# REPOSITORY_DIR: Der Name des BORG-Repositories

###################################################################################################
#
#    ____ ___ ____ _   _ _____ ____  _   _ _   _  ____ 
#   / ___|_ _/ ___| | | | ____|  _ \| | | | \ | |/ ___|
#   \___ \| | |   | |_| |  _| | |_) | | | |  \| | |  _ 
#    ___) | | |___|  _  | |___|  _ <| |_| | |\  | |_| |
#   |____/___\____|_| |_|_____|_| \_\\___/|_| \_|\____|
#
#                                                      
###################################################################################################
# 
# »»» Ab hier bitte keine Änderungen mehr vornehmen »»»
NEXTCLOUDDATEN=$(sudo -u www-data php $NPATH/occ config:system:get datadirectory)
NEXTCLOUDDB=$(sudo -u www-data php $NPATH/occ config:system:get dbname)
NEXTCLOUDDBUSER=$(sudo -u www-data php $NPATH/occ config:system:get dbuser)
NEXTCLOUDDBPASSWORD=$(sudo -u www-data php $NPATH/occ config:system:get dbpassword)
sudo -u www-data php $NPATH/occ maintenance:mode --on
if [ ! -d "/var/log/borg" ]; then
  mkdir -p /var/log/borg
fi
if [ ! -d "/sicherung/sql" ]; then
  mkdir -p /sicherung/sql
fi
mkdir -p /var/log/borg /backup/sql
LOG="/var/log/borg/$(date +%y%m%d-%H%M)-backup.log"
REPOSITORY="ssh://${BACKUP_USER}@${BACKUP_USER}.your-storagebox.de:23/./backup/${REPOSITORY_DIR}"
errorecho() { cat <<< "$@" 1>&2; }
exec > >(tee -i "${LOG}")
exec 2>&1
echo "###### Backup gestartet: $(date) ######"
echo ""
echo "Dienste werden gestoppt ..."
systemctl stop $WEBSERVER.service php$PHPVERSION-fpm.service redis-server.service
echo ""
echo "Datenbanksicherung wird erstellt ..."
mariadb-dump --single-transaction --routines -h localhost -u$NEXTCLOUDDBUSER -p$NEXTCLOUDDBPASSWORD -e $NEXTCLOUDDB > /sicherung/sql/nextcloud.sql
echo ""
echo "Datenbankgröße ermitteln ..."
mysql -u$NEXTCLOUDDBUSER -p$NEXTCLOUDDBPASSWORD -e "SELECT table_schema 'DB',round(sum(data_length+index_length)/1024/1024,2) 'Size (MB)' from information_schema.tables WHERE table_schema='$NEXTCLOUDDB';"
systemctl stop mariadb.service
echo ""
echo "Übertrage Dateien ..."
borg create -v --stats                   \
    $REPOSITORY::$(date +%y%m%d-%H%M)    \
    /root                                \
    /etc                                 \
    /var/www                             \
    /home                                \
    $NEXTCLOUDDATEN                      \
    /sicherung/sql                          \
    --exclude /backup                    \
    --exclude /dev                       \
    --exclude /proc                      \
    --exclude /sys                       \
    --exclude /var/run                   \
    --exclude /run                       \
    --exclude /lost+found                \
    --exclude /mnt                       \
    --exclude /var/lib/lxcfs
echo ""
borg prune --progress --stats $REPOSITORY --keep-within=7d --keep-weekly=4 --keep-monthly=6
echo ""
echo "Dienste werden gestartet ..."
systemctl restart mariadb.service redis-server.service php$PHPVERSION-fpm.service $WEBSERVER.service
echo ""
echo "Aufräumen ..."
rm -f /sicherung/sql/nextcloud.sql
sudo -u www-data php $NPATH/occ maintenance:mode --off
echo ""
echo "###### Backup beendet: $(date) ######"
mail -s "CLOUD-Backup" -a "FROM: $VORNAME $NACHNAME <$EMAIL>" $EMAIL < $LOG
exit 0
# (c) Carsten Rieger IT-Services
# https://www.c-rieger.de