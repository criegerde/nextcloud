#!/usr/bin/env bash
#
# Nextcloud Datenbanksicherung
# Datenbanktyp: postgreSQL oder MariaDB
# 
#########################################
NPATH="/var/www/nextcloud"
SPATH="/home/<user>/sicherung/sql"
SNPATH="/home/<user>/sicherung/nextcloud"
SDATE="$(date +%d.%m-%H.%M)_nextcloud.sql"
########################################
if [ ! -d $SPATH ]; then
  mkdir -p $SPATH
fi
if [ ! -d $SNPATH ]; then
  mkdir -p $SNPATH
fi
NEXTCLOUDDBTYPE=$(sudo -u www-data php $NPATH/occ config:system:get dbtype)
NEXTCLOUDDATEN=$(sudo -u www-data php $NPATH/occ config:system:get datadirectory)
NEXTCLOUDDB=$(sudo -u www-data php $NPATH/occ config:system:get dbname)
NEXTCLOUDDBUSER=$(sudo -u www-data php $NPATH/occ config:system:get dbuser)
NEXTCLOUDDBPASSWORD=$(sudo -u www-data php $NPATH/occ config:system:get dbpassword)
clear
echo " » Die Datenbanksicherung wird gestartet..."
if [ $NEXTCLOUDDBTYPE = "pgsql" ]; then
	PGPASSWORD="$NEXTCLOUDDBPASSWORD" pg_dump $NEXTCLOUDDB -h localhost -U $NEXTCLOUDDBUSER -f $SPATH/$SDATE
else
	mariadb-dump --single-transaction --routines -h localhost -u$NEXTCLOUDDBUSER -p$NEXTCLOUDDBPASSWORD -e $NEXTCLOUDDB > $SPATH/$SDATE
fi
	echo ""
	echo " » Die Datenbankgröße wird ermittelt..."
	echo -e "\033[32m » $(du -sh $SPATH/$SDATE | awk '{ print $1 }')\033[0m"
	echo ""
	echo " » Das Nextcloudverzeichnis wird gesichert..."
	echo -e "\033[32m » $(du -sh $SNPATH| awk '{ print $1 }')\033[0m"
	rsync -a $NPATH/ $SNPATH
	echo ""
exit 0